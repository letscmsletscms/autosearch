<?php
class ControllerCommonAutosearch extends Controller {
	public function index() {
		$this->load->language('common/autosearch');
		
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		
		
		$data['categories'] = array();
		$category_id=0;
		$results = $this->model_catalog_category->getCategories($category_id);
		$data['categories'][]=['name'=>'All','categpry_id'=>0];

		foreach ($results as $result) {
			$filter_data = array(
				'filter_category_id'  => $result['category_id'],
				'filter_sub_category' => true
			);

			$data['categories'][] = array(
				'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),				
				'category_id'=>$result['category_id']
			);
		}

		$data['text_autosearch'] = $this->language->get('text_autosearch');
		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->request->get['search'])) {
			$data['search'] = $this->request->get['search'];
		} else {
			$data['search'] = '';
		}

		return $this->load->view('common/autosearch', $data);
	}
	function AutoSearchAjax()
	{
		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		$this->load->language('common/autosearch');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/auto_search.css');
		$search=$_POST['search'];

		if(isset($_POST['category_id']) && !empty($_POST['category_id'])){
		$category_id=$_POST['category_id'];
		}else{
			$category_id='';
		}
		if(!empty($search) && isset($search)){
		$return=array();
		$filter_data = array(
				'filter_name'         => $search,
				'filter_tag'          => '',
				'filter_description'  => '',
				'filter_category_id'  => $category_id,
				'filter_sub_category' => '',
				'sort'                => '',
				'order'               => '',
				'start'               => '',
				'limit'               => ''
			);
		 //$cur_symble.number_format($product['price'],2),
			$cur_symble=$this->currency->getSymbolLeft($this->session->data['currency']);
			if(!isset($cur_symble) && $cur_symble=='')
			{	
			$cur_symble=$this->currency->getSymbolRight($this->session->data['currency']);
			}
			$products = $this->model_catalog_product->getProducts($filter_data);
			if(!empty($products))
			{

				$return['message']='<a class="btn-default auto_p_0 border_r_0" href="'.HTTPS_SERVER.'/index.php?route=product/search&search='.$search.'">'.$this->language->get('text_view_all').'</a>';
				
				foreach ($products as $key => $product) {
					if(isset($product['special']) && $product['special']){
						$price=$product['special'];
						$special=$product['price'];
					}else{
						$price=$product['price'];
						$special=0;
					}
					$return['products'][]=array(
						'product_id'	=> $product['product_id'],
						'name'			=> $product['name'],
						'image'			=> HTTPS_SERVER.'/image/'.$product['image'],
						'price'			=> $this->currency->format($this->tax->calculate($price, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
						'special'			=> $this->currency->format($this->tax->calculate($special, $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
						'href'        	=> $this->url->link('product/product', 'product_id=' . $product['product_id']),
						'manufacturer'  => $product['manufacturer'],
						'model'        	=> $product['model'],
					);
					
				}
			}else{
				$return['message']=$this->language->get('text_no_data_found');
			}
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($return));
	}

}