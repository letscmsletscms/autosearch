<?php
// Heading
$_['heading_title']    = 'Autosearch';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Autosearch module!';
$_['text_edit']        = 'Edit Autosearch Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Autosearch module!';